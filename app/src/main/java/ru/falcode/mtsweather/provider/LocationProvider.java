package ru.falcode.mtsweather.provider;

import android.annotation.SuppressLint;
import android.location.Location;
import android.location.LocationManager;

import java.util.List;

import ru.falcode.mtsweather.model.LatLng;

/**
 * Created by est_s on 3/1/2018.
 */

public class LocationProvider {

    @SuppressLint("MissingPermission")
    public static LatLng getLocation(LocationManager locationManager) {
        Location location = getLastKnownLocation(locationManager);
        Double lat, lon;
        try {
            lat = location.getLatitude();
            lon = location.getLongitude();
            return new LatLng(lat, lon);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return new LatLng(51.0, 0.0);
        }
    }

    private static Location getLastKnownLocation(LocationManager locationManager) {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            @SuppressLint("MissingPermission") Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        return bestLocation;
    }

}
