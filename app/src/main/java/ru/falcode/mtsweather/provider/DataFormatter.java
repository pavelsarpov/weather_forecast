package ru.falcode.mtsweather.provider;

import android.location.Address;
import android.location.Geocoder;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import ru.falcode.mtsweather.R;
import ru.falcode.mtsweather.interfaces.IGetContext;
import ru.falcode.mtsweather.model.CommonInfo;
import ru.falcode.mtsweather.model.WeatherModel;

public class DataFormatter {
    private IGetContext iGetContext;

    public DataFormatter(IGetContext iGetContext) {
        this.iGetContext = iGetContext;
    }

    public WeatherModel formatData(CommonInfo commonInfo) {
        String cloudCoverage = getCloudInfo(commonInfo);
        int source = getDayNightInfo(commonInfo);
        String apparentTemperature = String.format(Locale.US, "%.0f", commonInfo.getCurrently().getApparentTemperature() - 32 * 5 / 9) + " °C";
        String temperature = String.format(Locale.US, "%.0f", (commonInfo.getCurrently().getTemperature() - 32) * 5 / 9) + " °C";
        String summary = commonInfo.getCurrently().getSummary();
        String city = getCityInfo(commonInfo);
        String visibility = String.format(Locale.US, "%.0f", commonInfo.getCurrently().getVisibility() * 1.6) + " km";
        String humidity = String.format(Locale.US, "%.0f", commonInfo.getCurrently().getHumidity() * 100) + " %";
        return new WeatherModel()
                .setApparentTemperature(apparentTemperature)
                .setCloudCoverage(cloudCoverage)
                .setTemperature(temperature)
                .setSummary(summary)
                .setCity(city)
                .setVisibility(visibility)
                .setHumidity(humidity)
                .setSource(source);
    }

    private String getCityInfo(CommonInfo commonInfo) {
        List<Address> addresses;
        try {
            addresses = new Geocoder(iGetContext.getContext(), Locale.US).getFromLocation(commonInfo.getLatitude(), commonInfo.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
            return "-";
        }
        return addresses.get(0).getLocality();
    }

    /**
     * Получение информации о времени суток
     *
     * @param commonInfo
     * @return ссылку на соответствующий drawable
     */
    private int getDayNightInfo(CommonInfo commonInfo) {
        DateTimeZone tz = DateTimeZone.forID(commonInfo.getTimezone());
        DateTime dateTime = new DateTime(tz);
        if (dateTime.getHourOfDay() > 12 && dateTime.getHourOfDay() < 24)
            return R.drawable.day;
        else
            return R.drawable.night;
    }

    private String getCloudInfo(CommonInfo commonInfo) {
        double cloudCoveragePercent = commonInfo.getCurrently().getCloudCover();
        //cloud
        if (cloudCoveragePercent < 0.25) {
            return ("Low");
        } else if (cloudCoveragePercent < 0.5 && cloudCoveragePercent > 0.25)
            return ("Average");
        else if (cloudCoveragePercent > 0.5 && cloudCoveragePercent < 0.75)
            return ("High");
        else return ("Heavy");
    }
}
