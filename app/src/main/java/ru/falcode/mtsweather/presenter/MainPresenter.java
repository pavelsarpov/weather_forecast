package ru.falcode.mtsweather.presenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.falcode.mtsweather.interfaces.IGetContext;
import ru.falcode.mtsweather.interfaces.IUpdateWeather;
import ru.falcode.mtsweather.model.CommonInfo;
import ru.falcode.mtsweather.model.LatLng;
import ru.falcode.mtsweather.provider.DataFormatter;

import static falcode.ru.newslist.news.api.APIKt.getDefaultRetrofitInstance;

public class MainPresenter {
    private IUpdateWeather iUpdateWeather;
    private IGetContext iGetContext;

    public MainPresenter(IUpdateWeather iUpdateWeather, IGetContext iGetContext) {
        this.iUpdateWeather = iUpdateWeather;
        this.iGetContext = iGetContext;
    }

    public void updateWeather(LatLng coordinates) {
        getDefaultRetrofitInstance()
                .getForecast(coordinates.getLat(), coordinates.getLon())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleForecast);
    }

    private void handleForecast(CommonInfo commonInfo) {
        iUpdateWeather.updateWeather(new DataFormatter(iGetContext).formatData(commonInfo));
    }

}