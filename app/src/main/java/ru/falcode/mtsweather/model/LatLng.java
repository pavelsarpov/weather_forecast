package ru.falcode.mtsweather.model;

public class LatLng {
    private Double lat, lon;

    public LatLng(Double lat, Double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

}