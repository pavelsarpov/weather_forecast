package ru.falcode.mtsweather.model;

/**
 * Created by est_s on 3/1/2018.
 */

public class WeatherModel {
    private String cloudCoverage;
    private int source;
    private String apparentTemperature;
    private String temperature;
    private String summary;
    private String city;
    private String visibility;
    private String humidity;

    public String getCloudCoverage() {
        return cloudCoverage;
    }

    public WeatherModel setCloudCoverage(String cloudCoverage) {
        this.cloudCoverage = cloudCoverage;
        return this;
    }

    public int getSource() {
        return source;
    }

    public WeatherModel setSource(int source) {
        this.source = source;
        return this;

    }

    public String getApparentTemperature() {
        return apparentTemperature;
    }

    public WeatherModel setApparentTemperature(String apparentTemperature) {
        this.apparentTemperature = apparentTemperature;
        return this;

    }

    public String getTemperature() {
        return temperature;
    }

    public WeatherModel setTemperature(String temperature) {
        this.temperature = temperature;
        return this;

    }

    public String getSummary() {
        return summary;
    }

    public WeatherModel setSummary(String summary) {
        this.summary = summary;
        return this;

    }

    public String getCity() {
        return city;
    }

    public WeatherModel setCity(String city) {
        this.city = city;
        return this;

    }

    public String getVisibility() {
        return visibility;
    }

    public WeatherModel setVisibility(String visibility) {
        this.visibility = visibility;
        return this;

    }

    public String getHumidity() {
        return humidity;
    }

    public WeatherModel setHumidity(String humidity) {
        this.humidity = humidity;
        return this;

    }
}
