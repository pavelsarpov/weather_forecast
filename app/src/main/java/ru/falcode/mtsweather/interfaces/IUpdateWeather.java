package ru.falcode.mtsweather.interfaces;

import ru.falcode.mtsweather.model.WeatherModel;

/**
 * Created by est_s on 2/27/2018.
 */

public interface IUpdateWeather {
    void updateWeather(WeatherModel wm);
}
