package ru.falcode.mtsweather.interfaces;

import android.content.Context;

/**
 * Created by est_s on 2/27/2018.
 */

public interface IGetContext {
    Context getContext();
}
