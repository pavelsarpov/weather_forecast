package ru.falcode.mtsweather.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ru.falcode.mtsweather.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MainFragment()).commit();
    }
}
