package ru.falcode.mtsweather.view;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ru.falcode.mtsweather.R;
import ru.falcode.mtsweather.interfaces.IGetContext;
import ru.falcode.mtsweather.interfaces.IUpdateWeather;
import ru.falcode.mtsweather.model.LatLng;
import ru.falcode.mtsweather.model.WeatherModel;
import ru.falcode.mtsweather.presenter.MainPresenter;
import ru.falcode.mtsweather.provider.LocationProvider;

import static android.support.v4.content.ContextCompat.checkSelfPermission;

public class MainFragment extends Fragment implements IUpdateWeather, IGetContext, ActivityCompat.OnRequestPermissionsResultCallback {
    TextView apparentTemperatureTv, humidityTv, visibilityTv, cloudTv, temperatureTv, summaryTv, cityTv;
    ImageView background;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        checkPermissions();
    }

    private void initViews(View view) {
        temperatureTv = view.findViewById(R.id.temperature);
        background = view.findViewById(R.id.background);
        cityTv = view.findViewById(R.id.city);
        summaryTv = view.findViewById(R.id.summary);
        apparentTemperatureTv = view.findViewById(R.id.apparentTemperature);
        humidityTv = view.findViewById(R.id.humidity);
        visibilityTv = view.findViewById(R.id.visibility);
        cloudTv = view.findViewById(R.id.cloud);
    }

    private void checkPermissions() {
        if (!isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION))
            requestPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        else
            getWeatherForecast();
    }

    private boolean isPermissionGranted(String perm) {
        int fine = checkSelfPermission(getActivity().getApplicationContext(), perm);
        return fine == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission(String perm) {
        requestPermissions(new String[]{perm}, 1);
    }

    /**
     * Получаем прогноз погоды
     */
    private void getWeatherForecast() {
        MainPresenter mainPresenter = new MainPresenter(this, this);
        LocationManager locationManager = (LocationManager) getActivity().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        LatLng coordinates = LocationProvider.getLocation(locationManager);
        mainPresenter.updateWeather(coordinates);
    }

    @Override
    public void updateWeather(WeatherModel wm) {
        background.setBackground(getResources().getDrawable(wm.getSource(), null));
        temperatureTv.setText(wm.getTemperature());
        cityTv.setText(wm.getCity());
        summaryTv.setText(wm.getSummary());
        apparentTemperatureTv.setText(wm.getApparentTemperature());
        humidityTv.setText(wm.getHumidity());
        visibilityTv.setText(wm.getVisibility());
        cloudTv.setText(wm.getCloudCoverage());
    }


    @Override
    public Context getContext() {
        return super.getContext();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getWeatherForecast();
            } else {
                checkPermissions();
            }
        }
    }
}
