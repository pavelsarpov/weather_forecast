package falcode.ru.newslist.news.api

import com.google.gson.GsonBuilder
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import ru.falcode.mtsweather.model.CommonInfo

fun getDefaultRetrofitInstance(): API {
    val gson = GsonBuilder().setLenient().create()
    val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl("https://api.darksky.net/forecast/b329ddc649cd17b57e8870e929f5f829/")
            .build()

    return retrofit.create(API::class.java)
}

interface API {
    @GET("{lat}, {lon}")
    fun getForecast(@Path("lat") lat: Double, @Path("lon") lon: Double): Observable<CommonInfo>
}
